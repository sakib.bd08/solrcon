<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Yaml\Parser;
class SolariumController extends Controller
{
    protected $client;

    public function __construct(\Solarium\Client $client)
    {
        $this->client = $client;
    }
    public function index()
    {
        return view('frontend.search');
    }
    public function ping()
    {
        // create a ping query
        $ping = $this->client->createPing();

        // execute the ping query
        try {
            $this->client->ping($ping);
            return response()->json('OK');
        } catch (\Solarium\Exception $e) {
            return response()->json('ERROR', 500);
        }
    }
    public function add(){

        $update = $this->client->createUpdate();

        $doc1 = $update->createDocument();
        $doc1->id = 1;
        $doc1->Company_s = 'ford';
        $doc1->personal = "asad";
        $doc1->license = 'l011';
    // and a second one
        $doc2 = $update->createDocument();
        $doc2->id = 2;
        $doc2->Company_s = 'bmw';
        $doc2->personal = 'mahbub';
        $doc2->license = 'l012';
        //  3
        $doc3 = $update->createDocument();
        $doc3->id = 3;
        $doc3->Company_s = 'tesla';
        $doc3->personal = 'russel';
        $doc3->license = 'l013';
        //4
        $doc4 = $update->createDocument();
        $doc4->id = 4;
        $doc4->Company_s = 'jeep';
        $doc4->personal = 'zea';
        $doc4->license = 'l014';
        //5
        $doc5 = $update->createDocument();
        $doc5->id = 5;
        $doc5->Company_s = 'land rover';
        $doc5->personal = 'tanvir';
        $doc5->license = 'l015';
        //6
        $doc6 = $update->createDocument();
        $doc6->id = 6;
        $doc6->Company_s = 'land cruser';
        $doc6->personal = 'sakib';
        $doc6->license = 'l016';
    // add the documents and a commit command to the update query
        $update->addDocuments(array($doc1, $doc2, $doc3, $doc4, $doc5, $doc6));
        $update->addCommit();
    // this executes the query and returns the result
        $result = $this->client->update($update);
        echo '<b>Update query executed</b><br/>';
        echo 'Query status: ' . $result->getStatus(). '<br/>';
        echo 'Query time: ' . $result->getQueryTime();
        dd('Request data does not have any files to import.');      

    } 

    ///search 
    public function send(Request $request)
    {
        
        $comp = preg_split("/[\s]+/", $request['search']);
        $a = '';
        foreach ($comp as $key => $value) {
            $a .=' AND Company_s:' .$value;  
        }
        //dd($a);
        $client = new Client();

        $response = $client->get('http://staging.ivivelabs.com:8983/solr/compliance/select', [
              'query' => [
                 'wt' => 'json',
                 'q' => '*:*' .$a,
                 'rows' => '10',
                 'debug'=>'false',
                 'echoParams'=>'all',
                'json.nl'=>'arrarr'
              ],
              'headers' => [ 'Content-Type' => 'application/json' ]
          ]);


          dd(json_decode($response->getBody(), true));

          
          dd($res);
        /*var_dump($res->getBody());
        var_dump($res->getBody()->getContents());*/
        $parser = new Parser();
        dd($parser);
        $parsed = $parser->json($res->getBody()); 
        $fetchedData="";
        if($parsed['response']['docs']){
          $fetchedData =  $parsed['response']['docs'][0]; 
          $arrayKeys = ["Pecos_ID", "License_number", "Issued_state","Hospital_name", "Hospital_location"];
          //dump($parsed['response']['docs'][0]);       
          foreach($arrayKeys as $key=>$value){
            if (!array_key_exists($value, $fetchedData)) {
                $fetchedData[$value] = $npi->$value;
            }
          }
        }
    }
}